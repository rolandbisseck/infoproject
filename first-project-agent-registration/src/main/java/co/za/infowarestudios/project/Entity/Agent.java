package co.za.infowarestudios.project.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;


@Entity
@Table(name = "tblAgent")
public class Agent {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;

    @Column(name = "Firstname")
    private String firstname;

    @Column(name = "Surname")
    private String surname;

    @Column(name = "MerchantID")
    private long merchantId;

    @Column(name = "Banking_details")
    private BankDetails bankDetails;

    @Column(name = "Cellphone")
    private int cellPhone;

    @Column(name = "IDnumber")
    private int IDNumber;

    @Column(name = "UserCode")
    private String userCode;

    @JsonIgnore
    private int pin;

    public Agent(String firstname, String surname, long merchantId, BankDetails bankDetails, int cellPhone, int IDNumber, String userCode, int pin) {
        this.firstname = firstname;
        this.surname = surname;
        this.merchantId = merchantId;
        this.bankDetails = bankDetails;
        this.cellPhone = cellPhone;
        this.IDNumber = IDNumber;
        this.userCode = userCode;
        this.pin = pin;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(long merchantId) {
        this.merchantId = merchantId;
    }

    public BankDetails getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(BankDetails bankDetails) {
        this.bankDetails = bankDetails;
    }

    public int getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(int cellPhone) {
        this.cellPhone = cellPhone;
    }

    public int getIDNumber() {
        return IDNumber;
    }

    public void setIDNumber(int IDNumber) {
        this.IDNumber = IDNumber;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }
}
    
