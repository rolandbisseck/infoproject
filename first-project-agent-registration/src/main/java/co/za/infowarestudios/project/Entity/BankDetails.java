package co.za.infowarestudios.project.Entity;

import javax.persistence.*;

/**
 * Created by ROLAND on 12/10/2015.
 */

@Entity
@Table(name = "tblBankDetails")
public class BankDetails
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bankID")
    private int id;

    @Column(name = "Bank_Name")
    private String bank_name;

    @Column(name= "Branch_name")
    public String branch_name;

    @Column(name = "Branch_code")
    public String branch_code;

    @Column(name = "Account_number")
    public String account_number;
    
    public BankDetails()
    {

    }

    public BankDetails(String bank_name, String branch_name, String branch_code, String account_number) {
        this.bank_name = bank_name;
        this.branch_name = branch_name;
        this.branch_code = branch_code;
        this.account_number = account_number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public String getBranch_code() {
        return branch_code;
    }

    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }
}
